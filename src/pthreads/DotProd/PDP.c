#include <stdio.h>
//#include <sys/sysinfo.h>
#include <pthread.h> 
#include <stdlib.h>
#include <math.h>
#include "timer.h"

// The program takes an "exp" and "Thres" as inputs. So the matrix size is
// 2^exp. "Thres" is the chunk size of which is supplied to each thread finally
// to compute a serial dot product. 
//
// In function pdp if the input range  is less than threshold
// serial dot product is called, otherwise its range is split into two and two
// pthreads are spawned with a self recursive call. It waits for the threads to
// join and exits. 

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
  int L;    // lower-bound on range
  int H;    // upper-bound on range
  float rslt; // dot-product result
  int nthr // number of threads spawned
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
   fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
   fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
   exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
   if (argc != 3) Usage(argv[0]);
   Exp = strtol(argv[1], NULL, 10);  
   if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
   Thres = strtol(argv[2], NULL, 10);
   if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}  

double serdp(RNG rng) {
  int rslt = 0;
  for(int i=rng.L; i<=rng.H; ++i) 
  {
    C[i] = A[i] * B[i];
    rslt += C[i];
  }
  return rslt;
}

void* pdp(void *myrng) {

  RNG *rng;
  rng = (RNG *)myrng;
  int rc;

  if ((rng->H - rng->L) <= Thres) {
    rng->rslt = serdp(*rng);
    pthread_exit(NULL);
  }
  else {
    
    RNG rngL = *rng;
    RNG rngH = *rng;
    
    rngL.H = rng->L + (rng->H - rng->L)/2;
    rngH.L = rngL.H+1;

    pthread_t left_thread, right_thread;

    // spawing a thread to do left range dot product
    rc = pthread_create(&left_thread, NULL, pdp, (void *) &rngL);

    if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        exit(-1);
    }

    
    // spawing a thread to do right range dot product
    rc = pthread_create(&right_thread, NULL, pdp, (void *) &rngH);

    if (rc){
        printf("ERROR; return code from pthread_create() is %d\n", rc);
        exit(-1);
    }

    pthread_join(left_thread, NULL);
    pthread_join(right_thread, NULL);

    rng->nthr = rngL.nthr + rngH.nthr + 2;
    rng->rslt = rngL.rslt + rngH.rslt; // gathering the result from left and and right threads

    pthread_exit(NULL);
  }
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
  // Turn this on on Linux systems
  // On Mac, it does not work
  printf("This system has\
          %d processors configured and\
  	      %d processors available.\n",
          get_nprocs_conf(), get_nprocs());
  
  Get_args(argc, argv);  
  int Size = (int) pow(2, Exp);  // problem size calculated from the input exponential
  int rc;
  printf("Will do DP of %d sized arrays\n", Size);

  A = (float *) malloc(Size*sizeof(float)); // allocating the first array of the dot product
  B = (float *) malloc(Size*sizeof(float)); // allocating the second array of the dot product
  printf("Filling arrays now\n");
  srand(17); //Seed with 17
  for (int i=0; i<Size; ++i) {
    A[i] = rand()%16;
    B[i] = rand()%16;
  }

  RNG rng;
  rng.L = 0;         // lower bound of the range
  rng.H = Size-1;    // upper bound of the range
  rng.rslt = 0.0;    // initializing the result
  rng.nthr = 0;      // number of threads
  //printf("Serial dot product is %f\n", serdp(rng));
  
  double start;
  GET_TIME(start);   // timing the critical portion of the program

  printf("Now invoking parallel dot product\n");
  C = (float *) malloc(Size*sizeof(float));
  for(int i=0; i<Size; ++i) {
    C[i]=0;
  }
  //printf("Parallel DP = %f\n", pdp(rng));
  //
  pthread_t thread_id_pdp;
  rc = pthread_create(&thread_id_pdp, NULL, pdp, (void *) &rng);

  if (rc){
      printf("ERROR; return code from pthread_create() is %d\n", rc);
      exit(-1);
  }
  pthread_join(thread_id_pdp, NULL);

  double finish;
  GET_TIME(finish);

  double elapsed = finish - start;
  printf("\n");
  printf("The elapsed time is %e seconds\n", elapsed);

  printf("Dot Product = %f\n",(double)rng.rslt);
  printf("Total number of threads spawned = %d\n",rng.nthr);
  free(A);
  free(B);
  free(C);

  pthread_exit(NULL);
}
